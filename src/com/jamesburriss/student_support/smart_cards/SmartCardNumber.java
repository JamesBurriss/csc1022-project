package com.jamesburriss.student_support.smart_cards;

import com.jamesburriss.student_support.students.Name;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The SmartCardNumber class is a representation of a students serial number
 * This is made up of a students initials, date of birth and unique identifier
 *
 * @author James Burriss
 * @version 1.0, $Date: 26/04/2018
 */

public final class SmartCardNumber {
    /**
     * This represents a dash between the three elements of a smart cards number
     */
    private static final String DASH = "-";

    /**
     * This represents the max number the unique identifier can reach
     */
    private static final int UNIQUE_IDENTIFIER_MAX = 99;

    private static final String SCN_PATTERN = "[a-zA-Z]{2}" + DASH + "\\d{4}" + DASH + "\\d+";

    private static final Map<String, SmartCardNumber> CARD_NUMBERS = new HashMap<>();

    private static int currentUniqueIdentifier = 0;

    private final int issueYear;
    private final int uniqueIdentifier;
    private final String smartCardNumber;

    /**
     * @param studentName the students name
     * @param issueYear the year of the card being issued
     * @param uniqueIdentifier the final digits to guarantee uniqueness
     */
    private SmartCardNumber(Name studentName, int issueYear,
                            int uniqueIdentifier) {
        this.issueYear = issueYear;
        this.uniqueIdentifier = uniqueIdentifier;
        this.smartCardNumber = SmartCardNumber.buildSmartCardNumber(studentName, issueYear, uniqueIdentifier);
    }

    /**
     * Gets the smart card issue year
     *
     * @return the year of issue
     */
    public int getIssueYear() {
        return issueYear;
    }

    /**
     * Gets the unique serial identifier of the card
     *
     * @return uniqueIdentifier
     */
    public int getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    /**
     * Gets the smart card number
     *
     * @return smart card number
     */
    public String getSmartCardNumber() {
        return smartCardNumber;
    }

    /**
     * Gets the year of issue of the smart card
     *
     * @param issueDate the current date
     * @return the date of issue
     */
    private static int getIssueYear(Date issueDate) {
        final Calendar c = Calendar.getInstance();

        c.setTime(issueDate);

        return c.get(Calendar.YEAR);
    }

    /**
     * This acts as a helper method and builds the first part of the smart card number      *
     * @param studentName the students name
     * @param issueYear the year of issue
     * @return the first part of the smart card number
     */
    private static String buildSmartCardNumber(Name studentName, int issueYear) {
        return studentName.getInitials() + DASH + issueYear + DASH;
    }

    /**
     * This acts as a helper method and builds the whole smart card number
     *
     * @param studentName the students name
     * @param issueYear the issue year of smartcard
     * @param uniqueIdentifier final two digits to guarantee uniqueness
     * @return the full smart card number
     */
    private static String buildSmartCardNumber(Name studentName, int issueYear, int uniqueIdentifier) {
        return studentName.getInitials() + DASH + issueYear + DASH + String.format("%02d", uniqueIdentifier);
    }

    /**
     * This method generates the first part of the smart card with
     * the student names initials and issue date
     *
     * @param studentName the students name
     * @param issueDate the date of issue
     * @return the first part of smart card number
     */
    public static SmartCardNumber generateSmartCard(Name studentName, Date issueDate) {
        final int issueYear = getIssueYear(issueDate);
        final String cardNumberStart = SmartCardNumber.buildSmartCardNumber(studentName, issueYear);

        currentUniqueIdentifier = 0;

        return generateSmartCard(studentName, issueYear, cardNumberStart);
    }

    /**
     * This method generates the full smart card number
     * the student names initials and issue date and unique identifier
     *
     * @param studentName the students name
     * @param issueYear the date of issue
     * @param cardNumberStart the first two parts of the smart card number
     * @throws IllegalArgumentException if The card number does not match the correct syntax
     * @throws IllegalArgumentException if the issue year is in the future
     * @throws IllegalArgumentException if all unique identifiers have been used
     * @return the full smart card number
     */
    public static SmartCardNumber generateSmartCard(Name studentName, int issueYear, String cardNumberStart) {
        final String cardNumber = cardNumberStart + currentUniqueIdentifier;

        if (!cardNumber.matches(SCN_PATTERN))
            throw new IllegalArgumentException("The card number is not in the correct syntax" + cardNumber);

        final int timeNow = Calendar.getInstance().get(Calendar.YEAR);

        if (issueYear > timeNow)
            throw new IllegalArgumentException("Issue year is in the future" + issueYear);

        if (currentUniqueIdentifier > UNIQUE_IDENTIFIER_MAX)
            throw new IllegalArgumentException("Serial numbers have been exhausted" + currentUniqueIdentifier);

        if (CARD_NUMBERS.containsKey(cardNumber)) {
            currentUniqueIdentifier++;

            return generateSmartCard(studentName, issueYear, cardNumberStart);
        }

        CARD_NUMBERS.put(cardNumber, new SmartCardNumber(studentName, issueYear, currentUniqueIdentifier));

        return CARD_NUMBERS.get(cardNumber);
    }

    /**
     * String representation of a smart card number as per the specification
     * In the order: Student Initials, issue date and unique identifier
     *
     * @return the full smart card number
     */
    public String toString() {
        return smartCardNumber;
    }
}
