package com.jamesburriss.student_support.smart_cards;

import com.jamesburriss.student_support.students.Name;

import java.util.Date;

/**
 * The SmartCard class represents a students smart-card
 * It features a unique smart-card serial number and various student details
 * @author James Burriss
 * @version 1.0, $Date: 01/05/2018
 */

public final class SmartCard {
    private final Name studentName;
    private final Date dateOfIssue;
    private final String number;
    private final Date DOB;


    /**
     * This constructor creates a students smartcard form the students name, D-O-B
     * date of issue and the smart cards number. The smart card number is generated in
     * the SmartCardNumber class.
     *
     * Every smart card is unique due to the serial number
     *
     * @param studentName the name of the student
     * @param dateOfIssue the date of issue of smartcard
     * @param DOB the students date of birth
     */
    public SmartCard(Name studentName, Date dateOfIssue, Date DOB) {
        this.studentName = studentName;
        this.dateOfIssue = dateOfIssue;
        this.DOB = DOB;
        number = SmartCardNumber.generateSmartCard(studentName, dateOfIssue).getSmartCardNumber();
    }

    /**
     * This gets the data of issue of the smartcard
     * @return the date of issue
     */
    public Date getDateOfIssue() { return new Date(dateOfIssue.getTime()); }

    /**
     *  This gets the students name
     *  @return the students name
     */
    public Name getStudentName() { return studentName; }

    /**
     * This gets the smartcards serial number
     * @return the serial number
     */
    public String getNumber() { return number; }

    /**
     * This gets the students date of birth
     * @return the date if birth of student
     */
    public Date getStudentDOB() {
        return DOB;
    }

    /**
     * This method creates a printed smart card in the form shown on the spec
     * With assertions made on formatting based on the order written on spec
     */
    public String toString() {
        return studentName
                + " - " +  DOB
                + " - " +  number
                + " - " +  dateOfIssue;
    }
}