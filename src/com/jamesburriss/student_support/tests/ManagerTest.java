package com.jamesburriss.student_support.tests;

import com.jamesburriss.student_support.students.*;
import org.junit.Assert;
import org.junit.Test;

import static com.jamesburriss.student_support.Manager.*;

public class ManagerTest {

    @Test
    public void testTerminateStudent() throws Exception {
        Postgraduate postgraduate = new Postgraduate(new Name("James Burriss"), "20/02/1989");
        registerStudent(postgraduate);

        //newly registered student should be assigned first available student ID
        Assert.assertEquals("a0004", postgraduate.getStudentID().toString());
        terminateStudent(postgraduate.getStudentID());

        //after previous student was terminated new student should use recycled Id
        Undergraduate undergraduate1 = new Undergraduate(new Name("Evelin Lorn"), "18/3/1987");
        registerStudent(undergraduate1);
        Assert.assertEquals("a0004", undergraduate1.getStudentID().toString());
    }

    @Test
    public void testStudentCount() throws Exception {

        //there should be no instances of either type of student yet
        Assert.assertEquals(0, getStudentCountOfType(Postgraduate.class));
        Assert.assertEquals(0, getStudentCountOfType(Undergraduate.class));
        Assert.assertEquals(0, getStudentCountOfType(PostgraduateResearch.class));

        Postgraduate postgraduate = new Postgraduate(new Name("James Burriss"), "19/01/1998");
        registerStudent(postgraduate);

        Undergraduate undergraduate = new Undergraduate(new Name("James Burriss"), "20/02/1989");
        registerStudent(undergraduate);

        Postgraduate postgraduate1 = new Postgraduate(new Name("Maksymilian Ailen"), "18/3/1990");
        registerStudent(postgraduate1);

        PostgraduateResearch postgraduateResearch = new PostgraduateResearch(new Name("Reece Dodds"), "18/3/1990", PostgraduateResearch.readSupervisors());
        registerStudent(postgraduateResearch);


        //After students have registered it should increment the counters
        Assert.assertEquals(2, getStudentCountOfType(Postgraduate.class));
        Assert.assertEquals(1, getStudentCountOfType(Undergraduate.class));
        Assert.assertEquals(1, getStudentCountOfType(PostgraduateResearch.class));
    }

    @Test
    public void testRegisterStudent() throws Exception {
        Postgraduate postgraduate = new Postgraduate(new Name("James Burriss"), "19/01/1998");
        registerStudent(postgraduate);

        Undergraduate undergraduate = new Undergraduate(new Name("James Burriss"), "20/02/1989");
        registerStudent(undergraduate);

        PostgraduateResearch postgraduateResearch = new PostgraduateResearch((new Name("James Burriss")), "19/01/1998", PostgraduateResearch.readSupervisors());
        registerStudent(postgraduateResearch);

        //Check student has been assigned all necessary data

        // postgraduate research has been assigned a supervisor
        Assert.assertNotNull(postgraduateResearch.getSupervisor());
        Assert.assertNotNull(postgraduateResearch.getName());
        Assert.assertNotNull(postgraduateResearch.getSmartCard());

        Assert.assertNotNull(postgraduate.getName());
        Assert.assertNotNull(postgraduate.getSmartCard());
        Assert.assertNotNull(postgraduate.getStudentID());

        Assert.assertNotNull(undergraduate.getStudentID());
        Assert.assertNotNull(undergraduate.getName());
        Assert.assertNotNull(undergraduate.getSmartCard());

       // test amend student data
        Assert.assertEquals("a0006", undergraduate.getStudentID().toString());
        amendStudentData(undergraduate.getStudentID(), new StudentID() );

        //After new student ID is created the ID has been incremented
        Assert.assertEquals("a0008", undergraduate.getStudentID().toString());
    }

    @Test
    public void testAgeCheck() throws Exception {

        //this student is old enough to register
        Undergraduate undergraduate = new Undergraduate(new Name("James Burriss"), "20/02/1989");
        Assert.assertTrue(ageCheck(undergraduate.getStudentDOB(), 17));

        //this student is old enough to register
        Postgraduate postgraduate = new Postgraduate(new Name("James Burriss"), "19/01/1998");
        Assert.assertTrue(ageCheck(postgraduate.getStudentDOB(), 20));

        PostgraduateResearch postgraduateResearch = new PostgraduateResearch((new Name("James Burriss")), "19/01/1998", PostgraduateResearch.readSupervisors());
        Assert.assertTrue(ageCheck(postgraduateResearch.getStudentDOB(), 20));


        //this student is not old enough to register
        Undergraduate undergraduate1 = new Undergraduate(new Name("James Burriss"), "20/02/2005");
        Assert.assertFalse(ageCheck(undergraduate1.getStudentDOB(), 17));

        //this student is not old enough to register
        Postgraduate postgraduate1 = new Postgraduate(new Name("James Burriss"), "19/01/2000");
        Assert.assertFalse(ageCheck(postgraduate1.getStudentDOB(), 20));

        //this student is not old enough to register
        PostgraduateResearch postgraduater = new PostgraduateResearch((new Name("James Burriss")), "19/01/2000", PostgraduateResearch.readSupervisors());
        Assert.assertFalse(ageCheck(postgraduater.getStudentDOB(), 20));
    }
}