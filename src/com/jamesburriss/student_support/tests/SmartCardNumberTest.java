package com.jamesburriss.student_support.tests;

import com.jamesburriss.student_support.smart_cards.SmartCardNumber;
import com.jamesburriss.student_support.students.Name;
import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class SmartCardNumberTest {

    @Test
    public void testGetIssueYear() throws Exception {
        Name name = new Name("Jamess Burriss");
        SmartCardNumber scn = SmartCardNumber.generateSmartCard(name, new Date());
        int issueYear = scn.getIssueYear();
        Calendar calender = Calendar.getInstance();
        int now = calender.get(Calendar.YEAR);

        //issue time should be the same as current if checked immediately
        Assert.assertEquals(issueYear, now);
    }

    @Test
    public void testGetUniqueIdentifier() throws Exception {
        Name name = new Name("Jamesss Burriss");
        SmartCardNumber smartCardNumber = SmartCardNumber.generateSmartCard(name, new Date());
        int uniqueIdentifier = smartCardNumber.getUniqueIdentifier();

        //uniqueIdentifier has to be between 0 to 99 inclusive
        Assert.assertTrue(uniqueIdentifier >= 0);
        Assert.assertTrue(uniqueIdentifier <= 99);
    }

    @Test
    public void testToString() {
        Name name = new Name("Jamessss Burriss");
        SmartCardNumber smartCardNumber = SmartCardNumber.generateSmartCard(name, new Date());
        String smartCardNumberString = smartCardNumber.toString();
        String[] smartCardSegments = smartCardNumberString.split("-");

        Calendar calender = Calendar.getInstance();
        int now = calender.get(Calendar.YEAR);

        String initials = (smartCardSegments[0]);
        String issueDate = (smartCardSegments[1]);
        int serialNumber = Integer.parseInt(smartCardSegments[2]);

        //smart card should have 3 segments
        Assert.assertEquals(3, smartCardSegments.length);

        //initials show be upper case and match input
        Assert.assertEquals("JB", initials);

        //issue time should be the same as current if checked immediately
        Assert.assertEquals(Integer.toString(now), issueDate);


        //uniqueIdentifier has to be between 0 to 99 inclusive
        Assert.assertTrue(serialNumber <= 99);
    }

    @Test
    public void testIdentifier() throws NoSuchFieldException, IllegalAccessException {

        Name name = new Name("sames huss");
        SmartCardNumber smartCardNumber = SmartCardNumber.generateSmartCard(name, new Date());

        Name name1 = new Name("Jack Test");
        SmartCardNumber smartCardNumber1 = SmartCardNumber.generateSmartCard(name1, new Date());

        Name name2 = new Name("same hurriss");
        SmartCardNumber smartCardNumber2 = SmartCardNumber.generateSmartCard(name2, new Date());



        //first "Jack Test" with the lowest serial number and correct issue year
        Assert.assertEquals("JT-2018-00",smartCardNumber1.getSmartCardNumber());

        //first "SH" initial with serial number at 0
        Assert.assertEquals("SH-2018-00",smartCardNumber.getSmartCardNumber());

        //second "SH" initial with serial number incremented by 1
        Assert.assertEquals("SH-2018-01",smartCardNumber2.getSmartCardNumber());
    }
}
