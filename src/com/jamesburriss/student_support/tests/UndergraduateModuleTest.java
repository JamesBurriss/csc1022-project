package com.jamesburriss.student_support.tests;

import com.jamesburriss.student_support.modules.Module;
import com.jamesburriss.student_support.modules.PostgraduateModule;
import com.jamesburriss.student_support.modules.UndergraduateModule;
import com.jamesburriss.student_support.students.Name;
import com.jamesburriss.student_support.students.Undergraduate;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

public class UndergraduateModuleTest {

    @Test
    public void testGetUndergraduateModules() throws Exception {

        Module.populateModules();
        HashSet undergradModules = UndergraduateModule.getUndergraduateModules();
        HashSet postgradModules = PostgraduateModule.getPostgraduateModules();

        Undergraduate testUndergraduate = new Undergraduate(new Name("James Burriss"), "20/02/1989");
        testUndergraduate.setModules(UndergraduateModule.getUndergraduateModules());
        HashSet testUndergraduateModules = testUndergraduate.getModules();

        //Undergraduate should not have postgraduate modules
        Assert.assertEquals(testUndergraduateModules, undergradModules);
        Assert.assertNotEquals(testUndergraduateModules, postgradModules);
    }

}