package com.jamesburriss.student_support.tests;

import com.jamesburriss.student_support.students.StudentID;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

public class StudentIDTest {


    @Test
    public void testLetterCrossover() throws NoSuchFieldException, IllegalAccessException {
        Field field = StudentID.class.getDeclaredField("currentStudentIndex");

        field.setAccessible(true);

        field.set(null, 9999);

        StudentID id1 = new StudentID();

        //student should be assigned the last number suffix before moving on to new prefix
        Assert.assertEquals("a9999" ,id1.getIDString());

        StudentID id2 = new StudentID();
        //new student should be assigned a new letter prefix and new number suffix
        Assert.assertEquals("b0000" ,id2.getIDString());
    }

    @Test
    public void testGetIDString() throws Exception {
        String regex = "[a-zA-Z]\\d{4}";
        StudentID studentID = new StudentID();

        //student ID should have one character followed by four integers
        Assert.assertTrue(studentID.toString().matches(regex));
    }
}