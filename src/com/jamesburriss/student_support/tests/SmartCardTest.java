package com.jamesburriss.student_support.tests;

import com.jamesburriss.student_support.smart_cards.SmartCard;
import com.jamesburriss.student_support.students.Name;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.instanceOf;

public class SmartCardTest {
    @Test
    public void testGetDateOfIssue() throws Exception {
        Name name = new Name("James Burriss");
        SmartCard smartCard = new SmartCard(name, new Date(), new Date());

        //smart card issue date should be a date object
        Assert.assertThat(smartCard.getDateOfIssue(), instanceOf(Date.class));

        //smart card date of issue cannot be in the future
        Assert.assertFalse(smartCard.getDateOfIssue().after(new Date()));
    }

    @Test
    public void testToString() throws Exception {

        Name name = new Name("James Burriss");
        SmartCard smartCard = new SmartCard(name, new Date(), new Date());

        String smartCardString = smartCard.toString();

        String[] split = smartCardString.split(" - ");

        //student name should be first element of toString
        Assert.assertEquals(smartCard.getStudentName().toString(), split[0]);

        //student DOB should be second element of toString
        Assert.assertEquals(smartCard.getStudentDOB().toString(), split[1]);

        //smart card number should be third element of toString
        Assert.assertEquals(smartCard.getNumber().toString(), split[2]);

        //smart card date of issue should be last element of toString
        Assert.assertEquals(smartCard.getDateOfIssue().toString(), split[3]);


    }

}