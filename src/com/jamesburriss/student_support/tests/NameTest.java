package com.jamesburriss.student_support.tests;

import com.jamesburriss.student_support.students.Name;
import org.junit.Assert;
import org.junit.Test;

public class NameTest {
    @Test
    public void getFirstName() throws Exception {
        Name name = new Name("James Burriss");

        //first name should be the same as getFirstName
        Assert.assertEquals("James", name.getFirstName());
    }

    @Test
    public void getLastName() throws Exception {
        Name name = new Name("James Burriss");

        //second name should be the same as getFirstName
        Assert.assertEquals("Burriss", name.getLastName());
    }

    @Test
    public void getInitials() throws Exception {
        Name name = new Name("james burriss");

        //a students initials should be the same as name.getInitials and be uppercase
        Assert.assertEquals("JB", name.getInitials());
        Assert.assertNotEquals("jb", name.getInitials());
    }

    @Test
    public void testToString() {
        //name should be outputted the same way it is inputted
        Name name = new Name("James Burriss");
        Assert.assertEquals("James Burriss", name.toString());
    }

    @Test
    public void testEquals() {
        Name name = new Name("James Burriss");
        //name is the same
        Assert.assertTrue(name.equals(new Name("James Burriss")));

        //name should equal itself
        Assert.assertTrue(name.equals(name));

        //name should not equal null
        Assert.assertFalse(name.equals(null));

        //name does not equal "Jack"
        Assert.assertFalse(name.equals("Jack"));
    }

    @Test
    public void testHashCode() {
        Name name = new Name("James Burriss");
        Name name2 = new Name("James Burriss");
        Name name3 = new Name("Burriss James");

        //hashcode should be the same as the name is the same
        Assert.assertTrue(name.hashCode() == name2.hashCode());

        //hashcode should not be the same as the name is not the same
        Assert.assertFalse(name.hashCode() == name3.hashCode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameRegex() {
        //if name does not use only alphabet or empty it should throw exception
        Name name = new Name("1 2");
        Name nameNull = new Name(" ");
    }

    @Test(expected = NullPointerException.class)
    public void testNameNull() {
        //if name is null it should throw exception
        Name nameNull = new Name(null);
    }
}