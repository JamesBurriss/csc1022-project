package com.jamesburriss.student_support.tests;

import com.jamesburriss.student_support.modules.Module;
import com.jamesburriss.student_support.modules.PostgraduateModule;
import com.jamesburriss.student_support.modules.UndergraduateModule;
import com.jamesburriss.student_support.students.Name;
import com.jamesburriss.student_support.students.Postgraduate;
import com.jamesburriss.student_support.students.Undergraduate;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class PostgraduateModuleTest {
    @Test
    public void testGetPostgraduateModules() throws Exception {

        Module.populateModules();
        HashSet postgradModules = PostgraduateModule.getPostgraduateModules();
        HashSet undergradModules = UndergraduateModule.getUndergraduateModules();

        Postgraduate postgraduate = new Postgraduate(new Name("James Burriss"), "20/02/1989");
        postgraduate.setModules(PostgraduateModule.getPostgraduateModules());
        HashSet testPostgraduateModules = postgraduate.getModules();

        //Postgraduate should not have undergraduate modules
        Assert.assertEquals(testPostgraduateModules, postgradModules);
        Assert.assertNotEquals(testPostgraduateModules, undergradModules);

    }
}