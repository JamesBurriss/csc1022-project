package com.jamesburriss.student_support.students;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Random;
import java.util.Scanner;


    /**
     * PostgraduateResearch class is an implementation of a postgraduate research student
     *
     * @author James Burriss
     *
     * @version 1.0, $Date: 26/04/2018
     */

    final public class PostgraduateResearch extends AbstractStudent {
        private  String supervisor;
        public PostgraduateResearch(Name name, String DOB, String supervisor) throws ParseException {
            super(name, DOB);

            this.supervisor = supervisor;
        }

        /**
         * The file path of the supervisors data file
         */
        private static final String PATH = "src/supervisors.txt";

        /**
         * The maximum index of supervisors in the data file
         */
        private static final int UPPER_BOUND = 3;

        /**
         * The lowest index of supervisors in the data file
         */
        private static final int LOWER_BOUND = 0;


        /**
         * This method reads the supervisors from the data file
         * Random object is used to assign a random supervisor to a student
         *
         * @return a random supervisor
         * @throws FileNotFoundException if file cannot be read or located
         */
        public static String readSupervisors() throws FileNotFoundException {

            Scanner scanner = new Scanner(new File(PATH));

            int i = 0;
            String[] supervisors = new String[4];
            while (scanner.hasNextLine()) {
                supervisors[i] = scanner.nextLine();
                i++;
            }
            scanner.close();

            Random r = new Random();
            int s = r.nextInt(UPPER_BOUND - LOWER_BOUND) + LOWER_BOUND;

            String supervisor = supervisors[s];

            return supervisor;
        }

        /**
         * This gets the supervisor assigned above
         *
         * @return a random supervisor
         */
        public String getSupervisor() {
            return supervisor;
        }
    }

