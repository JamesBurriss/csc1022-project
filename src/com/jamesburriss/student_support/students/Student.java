package com.jamesburriss.student_support.students;

import com.jamesburriss.student_support.modules.Module;
import com.jamesburriss.student_support.smart_cards.SmartCard;

import java.util.Date;
import java.util.HashSet;

/**
 * This interface represents a default student
 *
 * @author James Burriss
 * @version 1.0, $Date: 02/05/2018
 */

public interface Student {

    /**
     * This gets the students ID
     *
     * @return the students ID
     */
    StudentID getStudentID();

    /**
     * This gets the current students ID
     * @param studentID the current StudentID object
     */
    void setStudentID(StudentID studentID);

    /**
     * This gets a students smart card
     *  @return the students smart card
     */
    SmartCard getSmartCard();

    /**
     * This sets the students smart card
     * @param smartCard the students smart card
     */
    void setSmartCard(SmartCard smartCard);

    /**
     * This gets the students name
     * @return the students name
     */
    Name getName();

    /**
     * This gets a students modules based on whether
     * they are undergrad or postgrad
     *
     * @return the students modules
     */
    HashSet<Module> getModules();

    /**
     * This sets the modules of a student
     *
     * @param modules the modules of a student
     */
    void setModules(HashSet<Module> modules);

    /**
     * This sets the students name
     *
     * @param name the name of the student
     */
    void setName(Name name);

    /**
     * This gets the students Date of Birth
     *
     * @return the students Date of birth
     */
    Date getStudentDOB();
}
