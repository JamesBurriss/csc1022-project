package com.jamesburriss.student_support.students;

import com.jamesburriss.student_support.modules.Module;
import com.jamesburriss.student_support.smart_cards.SmartCard;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

/**
 * AbstractStudent class creates an abstract implementation of a student
 *
 * @author James Burriss
 * @version 1.0, $Date: 01/05/2018
 */

abstract class AbstractStudent implements Student {
    private HashSet<Module> modules;
    private StudentID studentID;
    private SmartCard smartCard;
    private Name name;
    private Date DOB;


    AbstractStudent(Name name, String DOB) throws ParseException {
        this.name = name;

        DateFormat format = new SimpleDateFormat("d/m/y", Locale.ENGLISH); //Date of birth syntax: day/month/year
        this.DOB = format.parse(DOB);
    }

    /**
     * This gets the students Date of birth
     *
     * @return the students Date of birth
     */
    @Override
    public StudentID getStudentID() {
        return studentID;
    }

    /**
     * This sets the students ID
     *
     * @param studentID the current StudentID object
     */
    @Override
    public void setStudentID(StudentID studentID) {
        this.studentID = studentID;
    }

    /**
     * This gets the students date of birth
     *
     * @return the students Date of birth
     */
    @Override
    public Date getStudentDOB () {
        return DOB;
    }

    /**
     * This gets the students smart card
     *
     * @return the students smart card
     */
    @Override
    public SmartCard getSmartCard() {
        return smartCard;
    }

    /**
     * This sets the students smart card
     *
     * @param smartCard the students smart card
     */
    @Override
    public void setSmartCard(SmartCard smartCard) {
        this.smartCard = smartCard;
    }

    /**
     * This gets the students name
     *
     * @return the students name
     */
    @Override
    public Name getName() {
        return name;
    }

    /**
     *
     *
     * @param name the name of the student
     */
    @Override
    public void setName(Name name){
        this.name = name;
    }


    /**
     * This gets the modules of a student
     *
     * @return the modules of a student
     */
    @Override
    public HashSet<Module> getModules() {
        return modules;
    }

    /**
     * This sets the modules for a student
     *
     * @param modules the modules of a student
     */
    @Override
    public void setModules(HashSet<Module> modules) {
        this.modules = modules;
    }
}
