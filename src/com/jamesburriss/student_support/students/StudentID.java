package com.jamesburriss.student_support.students;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * This class creates a student's ID.
 * They are guaranteed to be unique
 *
 * @author James Burriss
 * @version 1.0, $Date: 01/05/2018
 */

public final class StudentID {
    /**
     * The minimum value a student id's index can be
     */
    private static final int MIN_STUDENT_INDEX = 0;
    /**
     * The maximum value a student id's index can be
     * Which is the number of letters in alphabet times the numbers per letter
     */
    private static final int MAX_STUDENT_INDEX = 260_000;

    /**
     *  The minimum value the number suffix can be
     *  (It cannot be a minus number)
     */
    private static final int MIN_NUMBER_SUFFIX = 0;

    /**
     * Generic english alphabet used for first value of ID
     */
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    /**
     * The ID needs to have a character prefix and 4 following digits
     */
    private static final String ID_REGEX = "[a-zA-Z]\\d{4}";

    private char letterPrefix;
    private int numberSuffix;
    private String idString;

    private static int currentStudentIndex = MIN_STUDENT_INDEX;

    private static final HashSet<String> STUDENT_IDS = new HashSet<>();
    private static final ArrayList<String> recycledIDs = new ArrayList<>();

    /**
     * Constructor which buildings students ID number
     * Uses letter prefix and 4 following numbers
     * currentStudentIndex is incremented to ensure every ID created is unique
     *
     * If a student has left the university it will issue the recycled ID first
     * before creating a new one
     */
    public StudentID() {
        if (recycledIDs.size() > 0) {
            this.idString = recycledIDs.get(0);
            recycledIDs.remove(0); //index at oldest// recycled ID
        } else {
            this.letterPrefix = buildLetterPrefix();
            this.numberSuffix = buildNumberSuffix();
            this.idString = buildIDString(letterPrefix, numberSuffix);
            StudentID.currentStudentIndex++;
        }
    }

    /**
     * This gets the letter part of the StudentID
     * @return the letter prefix
     */
    public char getLetterPrefix() {
        return letterPrefix;
    }

    /**
     * This gets the four numbers in the suffix
     * @return the ID's number suffix
     */
    public int getNumberSuffix() {
        return numberSuffix;
    }

    /**
     * This gets the whole Student ID
     * @return the students ID
     */
    public String getIDString() {
        return idString;
    }


    /**
     * This method adds an old student ID to a list of recycled ID to be given to new
     * student registrations
     *
     * @param studentID the recycled student ID
     */
    public static void addRecycledID(StudentID studentID) {
        recycledIDs.add(String.valueOf(studentID));
    }

    /**
     * This sets the ID's prefix to the current index in the alphabet
     * @return the char prefix of ID
     */
    public static char buildLetterPrefix() {

        return ALPHABET.charAt((int) Math.floor(currentStudentIndex / getMaxPerLetter()));

    }

    /**
     * This sets the ID's suffix to the current lowest available number
     * @return the suffix integer
     */
    public static int buildNumberSuffix() {
        return currentStudentIndex % getMaxPerLetter();
    }

    /**
     * This method builds the complete students ID from the prefix and suffix
     * @param letterPrefix the first part of the ID
     * @param numberSuffix the second part of the ID
     * @throws IllegalArgumentException if the ID does not meet the REGEX format
     * @throws IllegalArgumentException if the student ID becomes negative
     * @throws IllegalStateException if the student ID already exists
     * @return The final ID string of the student
     */
    private static String buildIDString(char letterPrefix, int numberSuffix) {
        String idString = letterPrefix + String.format("%04d", numberSuffix);

        if (!idString.matches(ID_REGEX))
            throw new IllegalArgumentException("The student ID is not in the correct format" + idString);

        if (numberSuffix < MIN_NUMBER_SUFFIX)
            throw new IllegalArgumentException("The student ID is too low" + idString);

        if (STUDENT_IDS.contains(idString)) {
            throw new IllegalStateException("The student ID already exists!" + idString);
        }

        STUDENT_IDS.add(idString);

        return idString;
    }

    /**
     * This method determines the greatest number of ID's per character
     * @return the number of ID's per character
     */
    private static int getMaxPerLetter() {
        return MAX_STUDENT_INDEX / ALPHABET.length();
    }

    /**
     * This creates a representation of the student ID
     * As per the specification it features the letter prefix and number suffix
     * which are guaranteed to be a unique representation
     * @return the Students ID
     */
    public String toString() {
        return idString;
    }
}