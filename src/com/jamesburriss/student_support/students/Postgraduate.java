package com.jamesburriss.student_support.students;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.*;

/**
 * Postgraduate class is an implementation of a postgraduate student
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 26/04/2018
 */

final public class Postgraduate extends AbstractStudent {

    public Postgraduate(Name name, String DOB) throws ParseException {
        super(name, DOB);
    }
}