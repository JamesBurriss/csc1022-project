package com.jamesburriss.student_support.students;

/**
 * Name class deals with full names
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 26/04/2018
 */

public final class Name {

    private final String firstName;
    private final String lastName;

    /**
     * Used to separate the first and last name
     */
    private static final String SPACE = " ";

    /**
     * Assumption that a name only contains alphabetic characters
     */
    private static final String REGEX = "[a-zA-Z]+";

    /**
     * @param fullName first and last name of student
     * @throws IllegalArgumentException if either first or last name is empty
     * @throws IllegalArgumentException if the name does not contain only alphabet characters
     */
    public Name(String fullName) {
        String[] splitName = fullName.split(SPACE);

        String firstName = splitName[0];
        String lastName = splitName[1];

        if (firstName.length() == 0 || lastName.length() == 0) {
            throw new IllegalArgumentException("Name is empty!"
                    + firstName + " " + lastName);
        }

        if (!firstName.matches(REGEX) || !lastName.matches(REGEX)) {
            throw new IllegalArgumentException("The name is not valid");
        }

        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Gets the first name of student
     *
     * @return first name
     */
    public String getFirstName() { return firstName; }

    /**
     * Gets the last name of a student
     *
     * @return last name
     */
    public String getLastName() { return lastName; }

    /**
     * This method retrieves the first letters of first and last name
     *
     * @return both initials together
     */
    public String getInitials() {
        final StringBuilder initials = new StringBuilder();

        initials.append(firstName.charAt(0)).append(lastName.charAt(0));

        return initials.toString().toUpperCase();
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof Name))
            return false;

        Name name = (Name) obj;

        return firstName.equals(name.firstName) && lastName.equals(name.lastName);
    }

    public int hashCode() {
        int hc = 17;

        hc = 31 * hc + firstName.hashCode();

        return 31 * hc + lastName.hashCode();
    }

    /**
     * This represents how a name would be presented in most of the world
     * Assumption made that there is not multiple first or second names
     *
     * @return full name
     */
    public String toString() {
        return firstName + SPACE + lastName;
    }
}
