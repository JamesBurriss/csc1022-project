package com.jamesburriss.student_support.students;

import java.text.ParseException;

/**
 * An undergraduate extension of an undergraduate student
 *
 * @author James Burriss
 * @version 1.0, $Date: 01/05/2018
 */

final public class Undergraduate extends AbstractStudent{
    public Undergraduate(Name name, String DOB) throws ParseException {
        super(name, DOB);
    }
}
