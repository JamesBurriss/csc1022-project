package com.jamesburriss.student_support.modules;

import java.util.HashSet;

/**
 * A postgraduate extension of an abstract student
 *
 * @author James Burriss
 * @version 1.0, $Date: 28/04/2018
 */

final public class PostgraduateModule extends Module {
    PostgraduateModule(String moduleCode, String moduleName, int moduleCredits) {
        super(moduleCode, moduleName, moduleCredits);
    }

    /**
     * This method retrieves only postgraduate modules and places
     * them in their own Hashset
     *
     * @return all postgraduate modules
     */
    public static HashSet<Module> getPostgraduateModules() {
        HashSet<Module> postgraduateModules = new HashSet<>();

        for (Module module : getModules()) {
            if (module instanceof PostgraduateModule) {
                postgraduateModules.add(module);
            }
        }

        return postgraduateModules;
    }
}
