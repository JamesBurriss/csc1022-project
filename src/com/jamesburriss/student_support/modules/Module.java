package com.jamesburriss.student_support.modules;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Module class will read a list of modules and data associated with them from a file
 * It creates a hash set of modules which have been split appropriately
 *
 * @author James Burriss
 * @version 1.0, $Date: 29/04/2018
 */

abstract public class Module {
    private static HashSet<Module> MODULES = new HashSet<>();

    /**
     * The filepath to the modules data file
     */
    private static final String PATH = "src/modules.txt";

    /**
     * A module code should have three alphabet characters and 4 numbers after
     */
    private static final String MODULE_CODE_PATTERN = "[a-zA-Z]{3}\\d{4}";

    /**
     * A module name should be only made up of alphabet characters
     */
    private static final String MODULE_NAME_PATTERN = "[a-zA-Z\\s]+";

    /**
     * A module's credits can only made up of two digits
     */
    private static final String MODULE_CREDITS_PATTERN = "\\d{2}";

    /**
     * REGEX that checks a section of the module code to see whether its undergraduate
     */
    private static final String UNDERGRADUATE_CODE = "(?i).*CSC1.*";

    /**
     * REGEX that checks a section of the module code to see whether its postgraduate
     */
    private static final String POSTGRADUATE_CODE = "(?i).*CSC8.*";

    private final String moduleCode;
    private final String moduleName;
    private final int moduleCredits;

    Module(String moduleCode, String moduleName, int moduleCredits) {
        this.moduleCode = moduleCode;
        this.moduleName = moduleName;
        this.moduleCredits = moduleCredits;
    }

    /**
     * This method reads the module data file and separates the file
     * using comma separated values. The module code is the first element of the line,
     * the module name is the second element and the number of credits the respective module
     * is worth is the third element. These are added to the modules Hash-set
     *
     * @throws FileNotFoundException if the file at PATH cannot be located or read
     * @throws IllegalArgumentException if the module code is not in the correct syntax
     * @throws IllegalArgumentException if the module name is not in the correct syntax
     * @throws IllegalArgumentException if the module credits is not in the correct syntax
     * @throws IllegalArgumentException if the module code is not applicable
     */
    public static void populateModules() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(PATH));

        while (scanner.hasNextLine()) {
            String[] moduleInfo = scanner.nextLine().split(", ");

            String moduleCode = moduleInfo[0];
            String moduleName = moduleInfo[1];
            int moduleCredits = Integer.parseInt(moduleInfo[2]);

            if (!moduleCode.matches(MODULE_CODE_PATTERN))
                throw new IllegalArgumentException("Module code is not is the correct format" + moduleCode);

            if (!moduleName.matches(MODULE_NAME_PATTERN))
                throw new IllegalArgumentException("Module name is not is the correct format" + moduleName);

            if (!Integer.toString(moduleCredits).matches(MODULE_CREDITS_PATTERN))
                throw new IllegalArgumentException("Module credits is not is the correct format" + moduleCredits);

            Module module;

            if (moduleCode.matches(UNDERGRADUATE_CODE)) { //checks if undergrad module
                module = new UndergraduateModule(moduleCode, moduleName, moduleCredits);
            } else if (moduleCode.matches(POSTGRADUATE_CODE)) {
                module = new PostgraduateModule(moduleCode, moduleName, moduleCredits);
            } else {
                throw new IllegalArgumentException("Module file contains wrong module codes");
            }

            Module.MODULES.add(module);
        }

        scanner.close();
    }

    /**
     * This gets all the modules in the hashset
     *
     * @return modules in Module hashset
     */
    static HashSet<Module> getModules() {
        return Module.MODULES;
    }

    /**
     * This method creates a printout of all module information
     *
     * @return each modules attached data
     */
    public String toString() {
        return moduleCode + " " + moduleName + " " + moduleCredits;
    }
}