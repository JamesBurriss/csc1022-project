package com.jamesburriss.student_support.modules;

import java.util.HashSet;

/**
 * An undergraduate extension of an abstract student
 *
 * @author James Burriss
 * @version 1.0, $Date: 28/04/2018
 */

public final class UndergraduateModule extends Module {
    public UndergraduateModule(String moduleCode, String moduleName, int moduleCredits) {
        super(moduleCode, moduleName, moduleCredits);
    }

    /**
     * This method retrieves only undergraduate modules and places
     * them in their own Hashset
     *
     * @return all undergraduate modules
     */
    public static HashSet<Module> getUndergraduateModules() {
        HashSet<Module> undergraduateModules = new HashSet<>();

        for (Module module : getModules()) {
            if (module instanceof UndergraduateModule) {
                undergraduateModules.add(module);
            }
        }

        return undergraduateModules;
    }
}