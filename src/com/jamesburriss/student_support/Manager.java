package com.jamesburriss.student_support;

import com.jamesburriss.student_support.modules.PostgraduateModule;
import com.jamesburriss.student_support.modules.UndergraduateModule;
import com.jamesburriss.student_support.smart_cards.SmartCard;
import com.jamesburriss.student_support.students.*;

import java.util.*;


/**
 * This represents a student manager class
 *
 * @author James Burriss
 * @version 1.0, $Date: 28/04/2018
 */

public final class Manager {

    private static final int MIN_UNDERGRADUATE_AGE = 17;
    private static final int MIN_POSTGRADUATE_AGE = 20;

    private static final HashMap<StudentID, Student> STUDENTS = new HashMap<>();

    /**
     * This method returns the number of students of the specified type that are currently enrolled.
     *
     * @param studentType the type of student. Postgraduate or Undergraduate
     * @return the number of instances of a specified student type
     */
    public static int getStudentCountOfType(Class<? extends Student> studentType) {
        int count = 0;

        for (Student student : STUDENTS.values()) {
            if (studentType.isInstance(student)) {
                count += 1;
            }
        }

        return count;
    }

    /**
     * This method registers a new student onto the system and allocates a student ID
     * a new smart card as per the specification. It also assign's a student their correct modules.
     *
     * The new student is added to a hashset of all students which are linked by their student ID
     *
     * @param student the new student object
     */

    public static void registerStudent(Student student) {
        if (STUDENTS.containsValue(student)) // Prevents students being registered more than once
            throw new IllegalArgumentException("Student: " + student.getName() + " has already been registered!");

        if (student instanceof Postgraduate) { // If postgraduate
            if (ageCheck(student.getStudentDOB(), MIN_POSTGRADUATE_AGE)) {
                student.setModules(PostgraduateModule.getPostgraduateModules());
            } else {
                throw new IllegalArgumentException("Student " + student.getName() + " is too young");
            }
        } if (student instanceof Undergraduate) { //If undergraduate
            if (ageCheck(student.getStudentDOB(), MIN_UNDERGRADUATE_AGE)) {
                student.setModules(UndergraduateModule.getUndergraduateModules());
            } else {
                throw new IllegalArgumentException("Student " + student.getName() + " is too young");
            }
        }
          else {
            if (ageCheck(student.getStudentDOB(), MIN_POSTGRADUATE_AGE)) {
            } else {
                throw new IllegalArgumentException("Student " + student.getName() + " is too young");
            }
        }
        student.setStudentID(new StudentID());
        student.setSmartCard(new SmartCard(student.getName(), new Date(), student.getStudentDOB()));
        STUDENTS.put(student.getStudentID(), student);
    }

    /**
     * Checks if age of student meets the requirements on specification
     * (At least 17 for Undergraduate) or (At least 20 for Postgraduate)
     *
     * @param date the date of birth for a given student
     * @param minimumAge minimum age a student can be to register
     * @return true if student is old enough
     */
    public static boolean ageCheck(Date date, int minimumAge) {
        final Calendar calendar = Calendar.getInstance();

        Date currentTime = new Date();
        if (date.after(currentTime))
            throw new IllegalArgumentException(
                    "The date of birth is is the future!");

        calendar.add(Calendar.YEAR, -minimumAge); //Subtracts minimum age from current year

        Date minAgeDate = calendar.getTime();

        return date.before(minAgeDate) || date.equals(minAgeDate);
    }

    /**
     * This method allows the user to change overwrite a students data
     *
     * @param studentID    the students previous ID
     * @param newStudentID the students new ID
     * @return the new student object with amended details
     */
    public static Student amendStudentData(StudentID studentID, StudentID newStudentID) {
        Student student = STUDENTS.get(studentID);

        student.setStudentID(newStudentID);
        return student;
    }

    /**
     * This method removes the student record associated with the given student number. In
     * effect, the student is leaving the University.
     *
     * It then free's up the old student ID to be able to be used
     *
     * @param studentID the current students ID
     */
    public static void terminateStudent(StudentID studentID) {
        StudentID.addRecycledID(studentID);

        STUDENTS.remove(studentID);
    }
}